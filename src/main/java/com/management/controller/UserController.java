package com.management.controller;

import com.management.constant.AppConstant;
import com.management.dto.request.CreateUserRequest;
import com.management.dto.request.UpdateUserRequest;
import com.management.dto.request.UserSearch;
import com.management.dto.response.ListUserResponse;
import com.management.model.User;
import com.management.repository.UserRepository;
import com.management.service.UserService;
import com.management.utils.PageResultOrigin;
import com.management.utils.SendRedirectUrlUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Controller
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final String LOG_SCREEN_NAME = "admin";
    private static final String PATH_VIEW_LIST_USER = "admin/user";
    private static final String PATH_ADD_USER = "admin/user_add";
    private static final String PATH_EDIT_USER = "admin/user_edit";
    private static final String USER_ATTRIBUTE = "user";

    private static final ModelMapper modelMapper = new ModelMapper();

    @GetMapping(value = {"/", "/login"})
    public String login(final HttpServletRequest request) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String anonymousUser = "anonymousUser";
        if (auth.getPrincipal() != null && !anonymousUser.equals(auth.getName())) {
            return "redirect:" + SendRedirectUrlUtils.getUrl(request, AppConstant.PATH_ADMIN_HOME);
        } else {
            return "login";
        }
    }

    @GetMapping("/search")
    public String searchUser(final Model model, @ModelAttribute final UserSearch userSearch,
                             final HttpSession session,
                             @RequestParam(value = AppConstant.STAY_PAGE_ATTRIBUTE, defaultValue = "false") final Boolean stayPage) {
        log.info("UserSearchModel: " + userSearch.toString());
        User user = new User();
        Page<ListUserResponse> page = null;
        String logActionName = "searchUser";
        try {
            // keep status search if stayPage == true
            if (stayPage == true) {
//                user = ModelMapperUtils.map((UserSearchModel) session.getAttribute(SEARCH_USER_SESSION), Users.class);
//                pageable = (Pageable) session.getAttribute(AppConstant.PAGEABLE);
            } else {
//                user = ModelMapperUtils.map(searchModel, Users.class);
//                session.setAttribute(SEARCH_USER_SESSION, searchModel);
//                session.setAttribute(AppConstant.PAGEABLE, pageable);
            }
            page = userService.searchUser(userSearch.getSearchName(), 0, 10);
//            final Page<ListUserResponse> page = new Page<ListUserResponse>(searchpaging, "");
//            List<UsersDto> content = page.getContent();
            List<ListUserResponse> listResult = page.getContent();
            PageResultOrigin<ListUserResponse> searchpaging = new PageResultOrigin<>(page, "");
            log.info("User size: " + listResult.size());
            model.addAttribute(AppConstant.STAY_PAGE_ATTRIBUTE, false);
            model.addAttribute(AppConstant.USERS_ATTRIBUTE, listResult);
            model.addAttribute(AppConstant.PAGE_ATTRIBUTE, searchpaging);
            model.addAttribute(AppConstant.CURRENT_USER_ID, userService.getUserLogin().getId());
            String pathReponse = PATH_VIEW_LIST_USER + AppConstant.RESULT_TABLE;
            log.info("Path response: " + pathReponse);
            return pathReponse;
        } catch (Exception e) {
            log.error(LOG_SCREEN_NAME + logActionName + "[{}]", e);
            return null;
        }
    }

    @GetMapping(value = "/admin/home")
    public String home(final Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userDto = null;
        String logActionName = "home";
        try {
            userDto = userService.getUserLogin();
        } catch (Exception e) {
            log.error(LOG_SCREEN_NAME + logActionName, e);
            model.addAttribute(AppConstant.RESULT_ATTRIBUTE, false);
            return "admin/home";
        }
        ListUserResponse userModel = modelMapper.map(userDto, ListUserResponse.class);
        model.addAttribute(USER_ATTRIBUTE, userModel);
        model.addAttribute(AppConstant.CURRENT_DATE_ATTRIBUTE, modelMapper.map(LocalDateTime.now(), String.class));
        log.info("path: " + "admin/home");
        return "admin/home";
    }

    @GetMapping("/list-user")
    public String registration(final Model model, @ModelAttribute final UserSearch userSearch,
                               @RequestParam(value = AppConstant.STAY_PAGE_ATTRIBUTE, defaultValue = "false") final Boolean stayPage,
                               final HttpSession session) {
        if (stayPage == false) {
            session.removeAttribute(AppConstant.SESSION_SEARCH_MODEL);
        }
        model.addAttribute(AppConstant.SEARCH_MODEL_ATTRIBUTE, userSearch);
        model.addAttribute(AppConstant.STAY_PAGE_ATTRIBUTE, stayPage);
        return PATH_VIEW_LIST_USER;
    }

    @GetMapping("/add")
    public String add(@ModelAttribute(value = USER_ATTRIBUTE) final CreateUserRequest user) {
        return PATH_ADD_USER;
    }

    @PostMapping("/addnew")
    public String save(final Model model,
                       @Valid @ModelAttribute(value = USER_ATTRIBUTE) final CreateUserRequest userModel,
                       final BindingResult bindingResult, final RedirectAttributes redirectAttributes, final Locale locale) {
        String LOG_SCREEN_NAME = "addAdminUser";
        String pathLog = "Path: " + PATH_ADD_USER;
        String logActionName = "createNewUser";
//        if (!bindingResult.hasErrors()) {
            User user = null;
            try {
                user = userRepository.findByUserNameAndDeletedIsFalse(userModel.getUserName());
                // not insert if user name is already use
                if (user != null) {
//                    model.addAttribute("userExist", messageSource.getMessage(AppConstant.MSG_USER_EXIST, null, locale));
                    log.info(pathLog);
                    return PATH_ADD_USER;
                } else {
                    if (userModel.getPassword().equals(userModel.getConPassword())) {

                        User userLogin = userService.getUserLogin();
                        User userDomain = modelMapper.map(userModel, User.class);
                        userDomain.setCreatedUpdatedDateBy(userLogin != null ? userLogin.getUserName() : null);
                        userDomain.setDeleted(false);
                        userDomain.setPassword(bCryptPasswordEncoder.encode(userModel.getPassword()));
                        userRepository.save(userDomain);

                    } else {
//                        model.addAttribute("validConfirmPassword",
//                                messageSource.getMessage(AppConstant.VALID_CONFIRM_PASSWORD, null, locale));
                        log.info(pathLog);
                        return PATH_ADD_USER;
                    }
                    redirectAttributes.addAttribute(AppConstant.STAY_PAGE_ATTRIBUTE, true);
                    model.addAttribute(AppConstant.RESULT_ATTRIBUTE, true);
                    return PATH_ADD_USER;
                }

            } catch (Exception e) {
                log.error(LOG_SCREEN_NAME, logActionName, e);
                model.addAttribute(AppConstant.RESULT_ATTRIBUTE, false);
                log.info(pathLog);
                return PATH_ADD_USER;
            }
//        }
//        return PATH_ADD_USER;
    }

    @GetMapping("/editForm")
    public String redirectEdit(@RequestParam(name = "id") final Long id, final Model model) {
        String pathLog = "Path: " + PATH_EDIT_USER;
        String LOG_SCREEN_NAME = "editAdminUser";
        String logActionName = "editUserForm";
        if (id != null) {
            try {
                Optional<User> optionalUser = userRepository.findById(id);
                User user = optionalUser.get();
                model.addAttribute(USER_ATTRIBUTE, modelMapper.map(user, CreateUserRequest.class));
                log.info(pathLog);
                return PATH_EDIT_USER;
            } catch (Exception e) {
                log.error(LOG_SCREEN_NAME, logActionName, e);
                model.addAttribute(AppConstant.RESULT_ATTRIBUTE, false);
                return PATH_EDIT_USER;
            }

        }
        model.addAttribute("error", "errorr");
        log.info(pathLog);
        return PATH_EDIT_USER;
    }

    @PostMapping("/edit")
    public String save(final Model model, @Valid @ModelAttribute(value = USER_ATTRIBUTE) final UpdateUserRequest userModel,
                       final BindingResult bindingResult, final RedirectAttributes redirectAttributes) {
        String LOG_SCREEN_NAME = "editAdminUser";
        log.info("userModel: ", userModel);
        String pathLog = "Path: " + PATH_EDIT_USER;
        String logActionName = "save";
        // validate input
        if (!bindingResult.hasErrors()) {
            User user = null;
            try {
                Optional<User> optionalUser = userRepository.findById(userModel.getId());
                user = optionalUser.get();
            } catch (Exception e) {
                log.error(LOG_SCREEN_NAME, logActionName, e);
                model.addAttribute(AppConstant.RESULT_ATTRIBUTE, false);
                return PATH_EDIT_USER;
            }

            if (user != null) {
                try {
                    User userLogin = userService.getUserLogin();
                    user.setFullName(userModel.getFullName());
                    user.setEmail(userModel.getEmail());
                    user.setUpdatedDateBy(userLogin != null ? userLogin.getUserName() : null);
                    userRepository.save(user);
                } catch (Exception e) {
                    log.error(LOG_SCREEN_NAME, logActionName, e);
                    model.addAttribute(AppConstant.RESULT_ATTRIBUTE, false);
                    log.info(pathLog);
                    return PATH_EDIT_USER;
                }
            }
        } else {
            log.info(pathLog);
            return PATH_EDIT_USER;
        }
        redirectAttributes.addAttribute(AppConstant.STAY_PAGE_ATTRIBUTE, true);
        model.addAttribute(AppConstant.RESULT_ATTRIBUTE, true);
        log.info(pathLog);
        return PATH_EDIT_USER;
    }

    @GetMapping("/signout")
    public String logout() {
        return "logout";
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

    @GetMapping("/check")
    public String check() {
        return "check";
    }

}
