package com.management.model;

import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@Getter
public class AbstractModel {
    protected LocalDateTime createdDate;
    protected LocalDateTime updatedDate;
    protected String createdBy;
    protected String updatedBy;
    protected String deletedBy;

    public void setUpdatedDate(final LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(final String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setDeletedBy(final String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public void setCreatedUpdatedDateBy(final String userName) {
        LocalDateTime now = LocalDateTime.now();

        createdBy = userName;
        createdDate = now;
        updatedBy = userName;
        updatedDate = now;
    }

    public void setUpdatedDateBy(final String userName) {
        LocalDateTime now = LocalDateTime.now();

        updatedBy = userName;
        updatedDate = now;
    }
}
