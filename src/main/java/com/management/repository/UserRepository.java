package com.management.repository;

import com.management.dto.response.ListUserResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.management.model.User;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT new com.management.dto.response.ListUserResponse(u.id, " +
            "u.userName, u.fullName, u.email, u.lastLogin) FROM User u " +
            "WHERE u.deleted = false AND (u.userName LIKE CONCAT('%',?1,'%'))" +
            "OR (u.fullName LIKE CONCAT('%',?1,'%') )")
    Page<ListUserResponse> findAllUserByCondition(String search, Pageable pageable);

    User findByUserNameAndDeletedIsFalse(String userName);
}
