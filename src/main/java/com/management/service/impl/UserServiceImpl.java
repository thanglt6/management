package com.management.service.impl;

import com.management.dto.response.ListUserResponse;
import com.management.dto.response.LoginResponse;
import com.management.dto.response.UserDetail;
import com.management.model.User;
import com.management.repository.UserRepository;
import com.management.service.UserService;
import com.management.service.jwt.JwtService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.management.dto.request.CreateUserRequest;
import com.management.dto.request.LoginRequest;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;

    //    @Autowired
//    private final UserMapper userMapper;
    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        try {
            if (!this.checkLogin(loginRequest)) {
                throw new RuntimeException("tai khoan hoac mat khau khong dung");
            }
            LoginResponse response = new LoginResponse();
            response.setToken(jwtService.generateTokenLogin(loginRequest.getUserName()));
            ModelMapper modelMapper = new ModelMapper();
            LoginResponse.UserLogin userLogin = modelMapper.map(userRepository.findByUserNameAndDeletedIsFalse(loginRequest.getUserName()), LoginResponse.UserLogin.class );
            response.setProfile(userLogin);

            return response;

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<ListUserResponse> searchUser(String search, int pageNo, int pageSize) {
        PageRequest pageable = PageRequest.of(pageNo, pageSize);
        Page<ListUserResponse> responses = userRepository.findAllUserByCondition(search, pageable);
        log.info("Page User successful : [{}]", responses);
        return responses;
    }

    @Override
    public void create(CreateUserRequest userDto) {
        try {
            User user = userRepository.findByUserNameAndDeletedIsFalse(userDto.getUserName());
            if (user != null) {
                throw new DuplicateKeyException("trung username");
            }
            userRepository.save(createUser(userDto));
            log.info("Create user successful with request: [{}]", userDto);

        } catch (Exception e) {
            log.error("exception : [{}]", e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void updateLastLogin(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUserLogin() {
        UserDetail userDetailsImpl = (UserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (userDetailsImpl != null) {
            return userDetailsImpl.getUser();
        }
        return null;
    }

    private User createUser(CreateUserRequest createUserDto) {
        User user = new User();
        user.setUserName(createUserDto.getUserName());
        user.setEmail(createUserDto.getEmail());
        user.setFullName(createUserDto.getFullName());
        user.setPassword(bCryptPasswordEncoder.encode(createUserDto.getPassword()));
        return user;
    }

    /**
     * Check user login.
     *
     * @param loginRequest loginRequest
     * @return boolean
     */
    private boolean checkLogin(LoginRequest loginRequest) {
        try {
            User user = userRepository.findByUserNameAndDeletedIsFalse(loginRequest.getUserName());
            if (user == null) {
                throw new NotFoundException("Not found User by userName : [{}] " + loginRequest.getUserName());
            }
            return bCryptPasswordEncoder.matches(loginRequest.getPassword(), user.getPassword());

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

}
