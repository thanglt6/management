package com.management.service.jwt;

import com.management.constant.AppConstant;
import com.management.dto.response.UserDetail;
import com.management.model.User;
import com.management.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author: hungpd
 * @version: 1.0
 */
@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//    	String LOG_SCREEN_NAME = "login";
//        String logActionName = "login";
//    	try {
    		log.debug("Authenticating user with username = {}", username.replaceFirst("@.*", "@.***"));
            username = username.replace("'", " ");
            final User user = userRepository.findByUserNameAndDeletedIsFalse(username);
            if (user == null) {
            	log.info("Cannot find username");
                throw new UsernameNotFoundException(username);
            }

            final List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(AppConstant.ROLE_ADMIN));
            return new UserDetail(user, authorities);
//		} catch (Exception e) {
//			LoggerUtils.error(LOG_SCREEN_NAME, logActionName, e);
//			return null;
//		}
        
    }
}
