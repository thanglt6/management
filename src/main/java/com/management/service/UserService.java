package com.management.service;

import com.management.dto.response.ListUserResponse;
import com.management.model.User;
import org.springframework.data.domain.Page;

import com.management.dto.request.CreateUserRequest;
import com.management.dto.request.LoginRequest;
import com.management.dto.response.LoginResponse;

public interface UserService {

    LoginResponse login(LoginRequest loginRequest);
    Page<ListUserResponse> searchUser(String search, int pageNo, int pageSize);

    void create(CreateUserRequest userDto);

    void updateLastLogin(User user);

    User getUserLogin();
}
