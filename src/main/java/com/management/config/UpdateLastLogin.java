package com.management.config;

import com.management.constant.AppConstant;
import com.management.dto.response.UserDetail;
import com.management.model.User;
import com.management.service.UserService;
import com.management.utils.SendRedirectUrlUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

/**
 * @author: thanglt
 * @version: 1.0
 */
@Component
@Slf4j
public class UpdateLastLogin implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private UserService userService;

    /**
     * Update last login
     */
    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication)
            throws IOException, ServletException {
        log.info("onAuthenticationSuccess(...) function!");

        UserDetail userDetailsImpl = (UserDetail) authentication.getPrincipal();
        User user = userDetailsImpl.getUser();
        user.setLastLogin(LocalDateTime.now());
        userService.updateLastLogin(user);
        redirectStrategy.sendRedirect(request, response, SendRedirectUrlUtils.getUrl(request, AppConstant.PATH_ADMIN_HOME));
    }
}
