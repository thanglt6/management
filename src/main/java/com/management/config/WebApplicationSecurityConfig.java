package com.management.config;

import com.management.constant.AppConstant;
import com.management.utils.SendRedirectUrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

import javax.servlet.http.HttpServletResponse;

/**
 * SecurityConfig configuration security for the authentication and authorization
 *
 * @author thanglt
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private UpdateLastLogin updateLastLogin;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
        return authenticationProvider;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder builder) throws Exception {
        builder.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                // Authorization
                .antMatchers("/login", "/signout", "/error", "/registration", "/check")
                .permitAll()
                .anyRequest()
                .hasAuthority(AppConstant.ROLE_ADMIN)
                // csrf
                .and()
                .csrf()
                .disable()
                // Login
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .failureHandler((request, response, authentication) -> {
                    response.setStatus(HttpServletResponse.SC_OK);
                    redirectStrategy.sendRedirect(request, response, SendRedirectUrlUtils.getUrl(request, "/login") + "?error=true");
                })
                .defaultSuccessUrl("/admin/home", true)
                .successHandler(updateLastLogin)
                // Logout
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutSuccessHandler((request, response, authentication) -> {
                    response.setStatus(HttpServletResponse.SC_OK);
                    redirectStrategy.sendRedirect(request, response, SendRedirectUrlUtils.getUrl(request, "/signout"));
                })
                .permitAll()
                // Exception
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new AjaxAwareAuthenticationEntryPoint("/login"));
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/fonts/**", "/favicon.png");
    }
}
