package com.management.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

public class SendRedirectUrlUtils {
	public static String getUrl(HttpServletRequest request, String path) {
		HttpRequest req = new ServletServerHttpRequest(request);
		
		HttpHeaders headers = req.getHeaders();
		
		String protocolHeader = headers.getFirst("X-Forwarded-Proto");
		if (!StringUtils.hasText(protocolHeader)) {
			return path;
		}
		
		UriComponentsBuilder builder = UriComponentsBuilder.newInstance();
		builder.scheme((StringUtils.tokenizeToStringArray(protocolHeader, ",")[0]));
		
		builder.host(req.getURI().getHost());
		
		
		String portHeader = headers.getFirst("X-Forwarded-Port");
		if (StringUtils.hasText(portHeader)) {
			builder.port(Integer.parseInt(StringUtils.tokenizeToStringArray(portHeader, ",")[0]));
		} else {
			builder.port(req.getURI().getPort());
		}

		URI location = builder.path(request.getContextPath() + "/" + path).build().toUri();
		
		
		return location.toString();
	}
	
}
