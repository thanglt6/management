package com.management.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListUserResponse {
    private Long id;
    private String userName;
    private String fullName;
    private String email;
    private LocalDateTime lastLogin;
}
