package com.management.dto.response;

import lombok.Data;

@Data
public class LoginResponse {
    private String token;
    private UserLogin profile;

    @Data
    public static class UserLogin {
        private Long id;
        private String userName;
        private String fullName;
        private String email;
        private boolean deleted;
    }
}
