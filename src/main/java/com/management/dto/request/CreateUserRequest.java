package com.management.dto.request;

import lombok.Data;

@Data
public class CreateUserRequest {
    private Long id;
    private String lastLogin;
    private String createdDate;

    private String updatedDate;

    private String createdBy;

    private String updatedBy;

    private String password;

    private String conPassword;

    private String userName;

    private String fullName;

    private String email;
}
