package com.management.dto.request;

import lombok.Data;

@Data
public class UpdateUserRequest {
    private Long id;
    private String lastLogin;

    private String password;

    private String userName;

    private String fullName;

    private String email;

    private String createdDate;

    private String lastModifiedDate;

    private String createdBy;

    private String lastModifiedBy;
}
