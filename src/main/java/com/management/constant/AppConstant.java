package com.management.constant;

public class AppConstant {

    public final static String PATH_ADMIN_HOME = "/admin/home";

    public static final String ROLE_ADMIN = "ADMIN";

    public static final String STAY_PAGE_ATTRIBUTE = "stayPage";

    public static final int LIMIT_PAGE = 10;
    public static final int MAX_PAGE_ITEM_DISPLAY = 10;

    public static final String PAGEABLE = "pageable";

    public static final String USERS_ATTRIBUTE = "users";

    public static final String PAGE_ATTRIBUTE = "page";

    public static final String CURRENT_USER_ID = "currentUserId";

    public static final String RESULT_TABLE = ":: result-table";

    public static final String RESULT_ATTRIBUTE = "result";

    public static final String CURRENT_DATE_ATTRIBUTE = "currentDate";

    public static final String SEARCH_MODEL_ATTRIBUTE = "searchModel";

    public static final String SESSION_SEARCH_MODEL = "sessionSearchModel";

}
