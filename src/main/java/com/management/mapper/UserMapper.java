//package com.management.mapper;
//
//import com.management.dto.response.LoginResponse;
//import org.mapstruct.Mapper;
//
//import com.management.model.User;
//import org.mapstruct.Mapping;
//
//@Mapper(componentModel = "cdi")
//public interface UserMapper {
//
//    @Mapping(source = "userName", target = "userName")
//    LoginResponse.UserLogin mapUserLogin(User user);
//}
