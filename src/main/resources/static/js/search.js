/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    $("document").ready(onSearchInStart()); 
    $('input').keypress(function (e) {
		    if (e.which == '13') {
		    	onSearch();
		    }
		});
    $(".button").button();
    setUiIconRow();
    $('a.ui-icon.ui-icon-trash').click(function(e){
        e.preventDefault();
    });
    adjustDivResultWidth();
    
    $("#legend").click(function() {
        runEffect();
        return false;
    });
    var status = '${status}';
    var message = '${message}';
              
    var hiddenResponse = '${hiddenResponse}';
    if(hiddenResponse !== null && hiddenResponse.length > 0){
        if(hiddenResponse === "ERR"){
            dialog_info("No data.","Result");
        }else if(hiddenResponse === "NO_DATA"){
            dialog_info("No data.","Result");
        }else if(hiddenResponse === "SUC"){
            if(status === "success"){
                dialog_successful(message);
            }else{                                    
               //dialog_successful("ค้นหาข้อมูล สำเร็จ");
            }
        }
    }
});
            
            
//pagination
function select_page_all(p){
    $("#start").val(p);
    $("#startKeepIn").val($("#start").val());
    doSearch('');
}

function onSearchInStart(){
	setOldToNew();
}
            
function onSearch(){
    doSearch('');
}
            
function doSearch(act){
    $("#act").val(act);           
    var params = $("form").serialize();
    var status = true;
    if(status){
        $('#loading').show();
        $.ajax({
			url : getActUrlForDoSearch(act),
			method : 'GET',
			contentType :'application/x-www-form-urlencoded;charset=UTF-8',
			data: params,
			 error : function(jqXHR){
				 $('#loading').hide();
				 if (jqXHR.status == 403) {
			            window.location.href = 'login';
			        }else{
		                errorMsg("Unexpected error.");
			        }
	            },
			success : function(data, textStatus, jqXHR) {
				$('#loading').hide();
				$('#result-table').replaceWith(data);
				$(".button").button();
			}
		})
		
    }
}

function showHiddenResponseDialog(responseHtml){
    var responseMessageObj = $('#hiddenResponse', responseHtml);
    if(responseMessageObj !== null && responseMessageObj.length > 0){
        if(responseMessageObj[0].value === "ERR"){
            location.href = "../common/error.jsp";
        }else if(responseMessageObj[0].value === "NO_DATA"){ 
            dialog_info("No data.","Result");
        }else if(responseMessageObj[0].value === "SUC"){      
        }
    }    
}

function clearInput(){
    onClear();
}
            
function onClear() { 
    $("form").trigger('reset');
    doSearch('');
}