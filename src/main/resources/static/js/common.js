//Ready
$(document).ready(function() {
    $("[required]").after("<span class='required'>*</span>");
});
$("a").click(function(){
	$("a").blur();
}); 
// Loading page
$(window).load(function() {
    //$('#loading').hide();
    //defaultTodayFromTo();
});
$(window).resize(function() {
    adjustDivResultWidth();
});
/*$(window).keydown(function(e){
    return preventEnter(e);
 });
 */
 function blockSpecialKey(event) {
    var regex = new RegExp("^[a-zA-Z0-9_ ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function blockSpecialKeyForEmail(event) {
    var regex = new RegExp("^[a-zA-Z0-9_.@ ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
}

function blockSpecialKeyAndAllowThai(event) {
    
    var regex = new RegExp("^[a-zA-Z0-9ก-๙_.* ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    
    
}
 
 function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) || charCode===45)
        return false;
    return true;
}
 
 function preventEnter(e){
     var code = e.keyCode || e.which;
    if(code == 13) {
      e.preventDefault();
      return false;
    }
    return true;
 }
 function preventEnter(e){
     var code = e.keyCode || e.which;
    if(code == 13) {
      e.preventDefault();
      return false;
    }
    return true;
 }

function errorMsg(message) {
    var txt = "<div class=\"ui-widget error\" style=\"font-size:medium\">" +
            "<p><span class=\"error\" style=\"float: left; margin-right: .3em;\"></span>" +
              message + "</p>" +
            "</div>";
    $("#error").html(txt);
    $("#error").dialog("open");
}

function dialog_confirm(form, title, msg) {
    var txt = "<p>" + msg + "</p>";
    $("#dialog_msg").dialog({
        title: title,
        autoOpen: false,
        modal: true,
        width: 400,
        buttons: [
            {
                text: "Confirm",
                click: function() {
                    $(this).dialog("close");
                    if (form == 'frmRole')
                    {
                        $('#loading').show();
                    }
                    //var params = $('#' + form).serialize();
                    //alert(params);
                    $('#' + form).submit();     // จะไปเรียกหน้า action.java เพื่อทำการ delete (important)
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                    $('#loading').hide();
                }
            }
        ]
    });
    $("#dialog_msg").html(txt);
    $("#dialog_msg").dialog("open");
}

function dialog_confirm_ajax(title, msg) {
    var txt = "<p>" + msg + "</p>";
    $("#dialog_msg").dialog({
        title: title,
        autoOpen: false,
        modal: true,
        width: 400,
        buttons: [
            {
                text: "Confirm",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                text: "Cancel",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
    $("#dialog_msg").html(txt);
    //$("#dialog_msg").dialog("open");
}

function dialog_inform(txt, title) {
    $("#dialog_msg").dialog({
        title: title,
        autoOpen: false,
        modal: true,
        width: 400,
        buttons: [
            {
                text: "OK",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
    $("#dialog_msg").html(txt);
    $("#dialog_msg").dialog({
        close: function(event, ui) {$(this).dialog("close");}
    });
    $("#dialog_msg").dialog("open");
}

function dialog_successful(msg) {
    var txt = "<p>" + msg + "</p>";
    dialog_inform(txt, "Complete");
}

function dialog_alert(msg) {
    var txt = "<div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">" +
            "<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>" +
            "<strong>Warning:</strong> " + msg + "</p>" +
            "</div>";
    dialog_inform(txt, "Warning");
}

function dialog_info(msg, title) {
    var txt = "<div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">" +
            "<p><span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>" +
            msg + "</p>" +
            "</div>";
    dialog_inform(txt, title);
}

function checkTxt(field, value) {
    if ((/^\s*$/).test(value)) {
        errorMsg("Please input: " + field + ".");
        return false;
    }
    return true;
}

function checkInputTxt(field, value) {
    if ((/^\s*$/).test(value)) {
        var msg = "Please input " + field + ".";
        dialog_alert(msg);
        return false;
    }
    return true;
}

function runEffect() {
    $("#search").toggle("blind", {}, 500);
}

function isValidUrl(url) {
    var urlPattern = /^(ht)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/
    return (urlPattern.test(url));
}

function checkSelectBox(field, idx) {
    if (idx <= 0) {
        //errorMsg("Please select "+field+".");
        var msg = "Please Select " + field + ".";
        dialog_alert(msg);
        return false;
    }
    return true;
}

function maxLength(obj, max, msg) {
    var len = 0;
    var objVal = getTxtObjValue(obj);
    len = objVal.length;
    if (len > max) {
        dialog_alert(msg);
        return false;
    }
    return true;
}

function getTxtObjValue(obj) {
    var objVal = obj.value;
    if (typeof objVal === "undefined") {
        objVal = obj.val();
    }
    return objVal;
}

function DeletData(id, form) {
    document.getElementById("ID").value = id;
    document.getElementById("Mode").value = "d";
    dialog_confirm(form, "Confirm Delete", "Are you sure for delete this record ?");

//    $('#' + form).submit();
}

function ChangeOrder(IDOrder, Order, IDChange, Change, form) {
    document.getElementById("IDOrder").value = IDOrder;
    document.getElementById("Order").value = Order;
    document.getElementById("IDChange").value = IDChange;
    document.getElementById("Change").value = Change;
    document.getElementById("Mode").value = "o";
    $('#' + form).submit();
}

function ChangePage(page, form)
{
    document.getElementById("Page").value = page;
    document.getElementById("Mode").value = 'v';
    $('#' + form).submit();
}

function isDateFormat(strDate) {
    var dateFormat = /^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/([2][0]|[1][9])\d{2})$/;
    return dateFormat.test(strDate);
}

function stringToDateWithHours(field, value, hh, mm) {
    if (isDateFormat(value)) {
        var temp = value.split("/");
        var hour = parseInt(hh, 10);
        var minute = parseInt(mm, 10);
        var thisDate = new Date();
        thisDate.setFullYear(temp[2]);
        thisDate.setMonth(temp[1] - 1);
        thisDate.setDate(temp[0]);
        thisDate.setHours(hour, minute, 0, 0);
        return thisDate;
    } else {
        dialog_alert(field + " is incorrect format (dd/MM/yyyy).");
        return null;
    }
}
function adjustDivResultWidth() {
    var windowWidth = parseInt($(window).width()) - 30;
  //  alert(windowWidth);
    $(".scrollTable").css({'width': windowWidth});
}

function clearTextEdit()
{
    document.getElementById("TitleEN").value = "";
    document.getElementById("TitleTH").value = "";
    document.getElementById("DetailTH").value = "";
    document.getElementById("DetailEN").value = "";
    document.getElementById("imgThumbnailAvatar").value = "";
    document.getElementById("imgAvatar").value = "";
//  
}

function showPreview(ele)
{
    $('#imgAvatar').attr('src', ele.value); // for IE
    if (ele.files && ele.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imgAvatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(ele.files[0]);
    }
}

function showThumbnailPreview(ele)
{
    $('#imgThumbnailAvatar').attr('src', ele.value); // for IE
    if (ele.files && ele.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
            $('#imgThumbnailAvatar').attr('src', e.target.result);
        }

        reader.readAsDataURL(ele.files[0]);
    }
}

function GenCSV(form)
{
    //alert(form);
    document.getElementById("act").value = "EXPORT";
    $('#' + form).submit();
}

function setFocusAfterDialogClose(dialogId, objFocus) {
    $("#" + dialogId).dialog({
        close: function(event, ui) {
//            alert(objFocus.attr("id"));
            objFocus.focus();
        }
    });
}
function setFocusAfterDialogMessageClose(objFocus) {
    setFocusAfterDialogClose("dialog_msg", objFocus);
}
function onActionComplete(status, message) {
    if (status != null && $.trim(status).length > 0 && message != null && $.trim(message).length > 0) {
        if (status == 'success') {
            //dialog_successful(message);
        } else {
        	message.replace("endLine", "\n");
            dialog_alert(message);
        }
    }
}
function onActionCompleteForReport(status, message) {
    if (status != null && $.trim(status).length > 0 && message != null && $.trim(message).length > 0) {
        if (status == 'success') {
            //dialog_successful(message);
        } else {
            //dialog_alert(message);
        }
    }
}
function validateInputDateFromTo() {
    var fromObj = $("#from");
    var fromHHObj = $("#from_hh");
    var fromMMObj = $("#from_mm");
    var toObj = $("#to");
    var toHHObj = $("#to_hh");
    var toMMObj = $("#to_mm");

    var hasDateFrom = $.trim(fromObj.val()).length > 0;
    var hasDateTo = $.trim(toObj.val()).length > 0;

    var fromDateDesc = "Start Date";
    var toDateDesc = "End Date";
    if (hasDateFrom && !hasDateTo) {
        dialog_alert("Please Input " + toDateDesc + ".");
        setFocusAfterDialogMessageClose(toObj);
        return false;
    }
    if (!hasDateFrom && hasDateTo) {
        dialog_alert("Please Input " + fromDateDesc + ".");
        setFocusAfterDialogMessageClose(fromObj);
        return false;
    }
    if (hasDateFrom && hasDateTo) {
        var fromDate = stringToDateWithHours(fromDateDesc, fromObj.val(), fromHHObj.val(), fromMMObj.val());
        var toDate = stringToDateWithHours(toDateDesc, toObj.val(), toHHObj.val(), toMMObj.val());
        if (fromDate == null) {
            setFocusAfterDialogMessageClose(fromObj);
            return false;
        }
        if (toDate == null) {
            setFocusAfterDialogMessageClose(toObj);
            return false;
        }
        if (fromDate > toDate) {
            dialog_alert(fromDateDesc + " Exceed " + toDateDesc + ".");
            setFocusAfterDialogMessageClose(fromObj);
            return false;
        }
    } else {
        fromHHObj.get(0).selectedIndex = 0;
        fromMMObj.get(0).selectedIndex = 0;
        toHHObj.get(0).selectedIndex = 0;
        toMMObj.get(0).selectedIndex = 0;
    }
    return true;
}

function validateInputDateFromToWithoutTime(fromObj,fromStr,toObj,toStr) {
    var hasDateFrom = !isEmptyStr(fromObj.val());
    var hasDateTo = !isEmptyStr(toObj.val());

    if(hasDateFrom && !hasDateTo){
        if(!checkInputTxt(toStr,toObj.val())){
            setFocusAfterDialogMessageClose(toObj);
            return false;
        }
    }
    if(!hasDateFrom && hasDateTo){
        if(!checkInputTxt(fromStr,fromObj.val())){
            setFocusAfterDialogMessageClose(fromObj);
            return false;
        }
    }
    if(hasDateFrom && hasDateTo){
        if(isStartDateOverEndDate(toObj.val(),toStr,fromObj.val(),fromStr)){
            setFocusAfterDialogMessageClose(fromObj);
            return false;
        }
    }

//    fromObj.blur();
//    toObj.blur();

    fromObj.focusout();
    toObj.focusout();
    return true;
}

function isTargetDateOverBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr) {
        var result = compareTargetDateToBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr)
        if (result == "GR") {
            return true;
        }
        dialog_alert(targetDateStr+" must be set date after"+baseDateStr);
    return false;
}
function isTargetDateOverOrEqualBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr) {
        var result = compareTargetDateToBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr)
        if (result == "GR" || result == "EQ") {
            return true;
        }
        dialog_alert(targetDateStr+" must be set date from"+baseDateStr);
    return false;
}
function isStartDateOverEndDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr) {
    //start date = targetDateVal
    //end date = baseDateVal
    //targetDateVal > baseDateVal ?
        var result = compareTargetDateToBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr)
        if (result == "GR") {
            dialog_alert(targetDateStr+" must not over from "+baseDateStr);
            return true;
        }
    return false;
}
function compareTargetDateToBaseDate(baseDateVal,baseDateStr,targetDateVal,targetDateStr) {
    var result = "";
    var baseDate = stringToDateWithHours(baseDateStr, baseDateVal, "00", "00");
    var targetDate = stringToDateWithHours(targetDateStr, targetDateVal, "00", "00");
    baseDate = baseDate.getTime();
    targetDate = targetDate.getTime();
    if (baseDate != null && targetDate != null) {
        if (targetDate > baseDate) {
            result = "GR";
        }else if (targetDate < baseDate) {
            result = "LE";
        }else if (targetDate === baseDate) {
            result = "EQ";
        }
    }
    return result;
}
function isValidEmail(email) {
    var urlPattern = /^[a-zA-Z0-9\+\._%\-\+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}(\.[a-zA-Z0-9][a-zA-Z0-9\-]{0,25})+$/
    return urlPattern.test(email);
}
function checkEmail(emailObj){
    if(!isValidEmail(emailObj.val())){
        errorMsg("Invalid format.");
        setFocusAfterDialogMessageClose(emailObj);
        return false;
    }
    return true;
}
function checkEmailAlert(emailObj){
    if(!isValidEmail(emailObj.val())){
        dialog_alert("Email Invalid");
        setFocusAfterDialogMessageClose(emailObj);
        return false;
    }
    return true;
}

function SearchFrom(form)
{
//    alert(document.getElementById("Device").value);
    document.getElementById("Mode").value = "v";
    document.getElementById("Page").value = "1";
    $('#loading').show();
//    alert(form);
    $('#' + form).submit();
}

function SearchFromVulcan(form)
{
//    alert(document.getElementById("Device").value);
//    document.getElementById("Mode").value = "v";
    $('#loading').show();
//    alert(document.getElementById("Application").value);
//    alert(form);
    $('#' + form).submit();
}


function setUiIconRow() {
    $(".ui-icon-pencil").button();
    $(".ui-icon-trash").button();
    $(".ui-icon-search").button();
    $(".ui-icon-circle-arrow-n").button();
    $(".ui-icon-circle-arrow-s").button();
    $(".ui-icon-locked").button();
}
function initialFileLink(){
    $('a.file-link').click(function (event){
        var url = $(this).attr("href");                    
        window.open(url); 
        event.preventDefault();
    });
}

function isValidDecimal(val) {
    var urlPattern = /^(\-)?[0-9]+([\.][0-9]+)?$/
    return urlPattern.test(val);
}

function isValidNumber(val) {
    var urlPattern = /^[0-9]+?$/
    return urlPattern.test(val);
}

function validInputDecimal(obj,objStr){
    if(!checkInputTxt(objStr,obj.val())){
        setFocusAfterDialogMessageClose(obj);
        return false;
    }else if(!isValidDecimal(obj.val())){
        dialog_alert("Invalid "+objStr+".");
        setFocusAfterDialogMessageClose(obj);
        return false;
    }
    return true;
}

function validInputNumber(objStr,obj){
    if(!checkInputTxt(objStr,obj.val())){
        setFocusAfterDialogMessageClose(obj);
        return false;
    }else if(!isValidNumber(obj.val())){
        dialog_alert("Invalid "+objStr+".");
        setFocusAfterDialogMessageClose(obj);
        return false;
    }
    return true;
}

function isEmptyStr(value) {
    return (/^\s*$/).test(value);
}

function dateToString(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;//January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){dd='0'+dd}
    if(mm<10){mm='0'+mm}
    return dd+'/'+mm+'/'+yyyy;
    //today.
}

function defaultDateTime(d,h,m){
    var today = dateToString();
    var startHH = "00";
    var startMM = "00"; 
    $("#"+d).val(today);
    $("#"+h).val(startHH);
    $("#"+m).val(startMM);    
}



function defaultTodayFromTo(){
    var today = dateToString();
    var startHH = "00";
    var startMM = "00";
    var endHH = "23";
    var endMM = "59";
    $("#from").val(today);
    $("#from_hh").val(startHH);
    $("#from_mm").val(startMM);
    $("#to").val(today);
    $("#to_hh").val(endHH);
    $("#to_mm").val(endMM);
}

function validInputText(obj,objStr,targetLength){
    if(!checkInputTxt(objStr,obj.val().trim())){
        setFocusAfterDialogMessageClose(obj);
        return false;
    }else{
        if(!validMaxLength(obj,objStr,targetLength)){
            return false;
        }
        /*if(!maxLength(obj,targetLength,obj.val().length+" ตัวอักษร -> ความยาวสูงสุดกำหนด "+objStr+" ("+targetLength+" ตัวอักษร).")){
            setFocusAfterDialogMessageClose(obj);
            return false;
        }*/
    }
    return true;
}

function validMaxLength(obj,objStr,targetLength){
    //alert(obj.val().length);
    if(!maxLength(obj,targetLength,obj.val().length+" Label -> max length "+objStr+" ("+targetLength+" Label).")){
        setFocusAfterDialogMessageClose(obj);
        return false;
    }
    return true;
}

function checkDropdownList(ddlObj, ddlStr){
    if(ddlObj.get(0).selectedIndex == 0){
        dialog_alert("Please choose "+ddlStr+".");
        setFocusAfterDialogMessageClose(ddlObj);
        return false;
    }
    return true;
}

function checkImageFile(field,value){
    //file = form.elements[name].value;
    if(value.length > 0){
        var ext = value.split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            dialog_alert(field+" Invalid image file.");
            return false;
        }
        /*var type = value.substr(value.lastIndexOf('.'));
         if((type != '.png')&&(type !='.PNG') && (type != '.jpg')&&(type !='.JPG')&&(type !='.JPEG')){
            dialog_alert(field+" ไม่ใช่ไฟล์รูปภาพที่รองรับ.");
            return false;
        }*/
    }else{
        dialog_alert("Please choose file "+field);
        return false;
    }
    return true;
}

function checkPngFile(field,value){
    if(value.length > 0){
        var ext = value.split('.').pop().toLowerCase();
        if($.inArray(ext, ['png']) == -1) {
            dialog_alert(field+" Invalid image file.(png)");
            return false;
        }
    }else{
        dialog_alert("Please choose file "+field);
        return false;
    }
    return true;
}

function checkPdfFile(field,value){
    if(value.length > 0){
        var ext = value.split('.').pop().toLowerCase();
        if($.inArray(ext, ['pdf']) == -1) {
            dialog_alert(field+" Invalid image file.(pdf)");
            return false;
        }
    }else{
        dialog_alert("Please choose file "+field);
        return false;
    }
    return true;
}

function validFileSize(fileObj,fileStr,targetSizeInBytes){
    try{
        //check whether browser fully supports all File API
         if (window.File && window.FileReader && window.FileList && window.Blob)
         {
             //get the file size and file type from file input field
             var fsize = fileObj[0].files[0].size;
             if(fsize > targetSizeInBytes)
             {
                 dialog_alert("Invalid file size : "+fileStr);
                 return false;
             }/*else{
                 alert(fsize +" bites\nYou are good to go!");
             }*/
         }/*else{
             alert("Please upgrade your browser, because your current browser lacks some new features we need!");
         }*/
    }catch(e){
        
    }
    return true;
}

function validImageDimension(fileObj,fileStr){
    try{
        if (window.File && window.FileReader && window.FileList && window.Blob)
         {
            var file, img;
            var _URL = window.URL || window.webkitURL;
            if ((file = fileObj[0].files[0])) {
                img = new Image();
                img.onload = function () {
                    //alert("Width:" + this.width + "   Height: " + this.height);//this will give you image width and height and you can easily validate here....
                    var w = this.width;
                    var h = this.height;
                    console.log("Width:" + w + "   Height: " + h);
                    if(w < 1800 && h < 2200){
                        dialog_alert("file size must not less than 180x220 -> w = "+w+" , h = "+h);
                        return false;
                    }
                };
                img.src = _URL.createObjectURL(file);
                
                alert(img.width);
            }
         }
        //var image  = readImage(fileObj);
        //alert(image);
        //alert("1");
        //alert(image.width);
        //alert("2");
        //alert(image.height);
        //alert(image.width);
        //$(photo).bind('load',doSomething());
        //$(img).load();
        //alert($(img).width());
        //alert($(img).height());
        /*var w = image.width;
        var h = image.height;
        if(w < 180 && h < 220){
            dialog_alert("ขนาดรูปภาพต้องไม่ต่ำกว่า 180x220 -> w = "+w+" , h = "+h);
            return false;
        }*/
    }catch(e){
        alert("err-1");
        alert(e);
    }
    return true;
}

function readImage(fileObj) {//HTML5
    var file = fileObj[0].files[0];
    //alert(file);

    var reader = new FileReader();
    var image  = new Image();

    try{
        reader.readAsDataURL(file);  
        reader.onload = function(_file) {
            image.src    = _file.target.result;              // url.createObjectURL(file);
            //var deferred = $.Deferred();
            image.onload = function() {
                var w = this.width,
                    h = this.height,
                    t = file.type,                           // ext only: // file.type.split('/')[1],
                    n = file.name,
                    s = ~~(file.size/1024) +'KB';
                //$('#uploadPreview').append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
                //alert(w);
                //alert(h);
                //deferred.resolve();
                if(w < 180 && h < 220){
                    dialog_alert("file size must not less than 180x220 -> w = "+w+" , h = "+h);
                    return false;
                }
            };
            image.onerror= function() {
                //alert('Invalid file type: '+ file.type);
            };      
        };
        //image.load();
        /*var w = image.width;
        var h = image.height;
        if(w < 180 && h < 220){
            dialog_alert("ขนาดไม่ต่ำกว่า 180x220 -> w = "+w+" , h = "+h);
            return false;
        }*/
        //setTimeout(function(){}, 2000);
        //while(!image.complete){setTimeout(readImage(fileObj), 2000);};
        //alert(image.complete);
    }catch(e){
        //alert("err-1");
        //alert(e);
    }
    return true;
}


function forceInputNumber(){
    //http://www.aspsnippets.com/Articles/Numeric-Validation-Accept-only-numeric-values-numbers-in-TextBox-using-jQuery.aspx
    /* this will work in all browsers(no jQuery needed) */
    if (!Array.indexOf) {
      Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0); i < this.length; i++) {
          if (this[i] == obj) {
            return i;
          }
        }
        return -1;
      }
    }

    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    $(".numeric").bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        return ret;
    });
    $(".numeric").bind("paste", function (e) {
        var _this = this;
        var res = true;
        var text = "";
        // Short pause to wait for paste to complete
        setTimeout( function() {
            text = $(_this).val();
            res = isNumberDigit(text);
            if(!res){
                $(_this).val('');
            }
        }, 100);
    });
    $(".numeric").bind("drop", function (e) {
        var _this = this;
        var res = true;
        var text = "";
        // Short pause to wait for paste to complete
        setTimeout( function() {
            text = $(_this).val();
            res = isNumberDigit(text);
            if(!res){
                $(_this).val('');
            }
        }, 100);
    });
}
function highlight(select){
	 $(select).css("background-color","#f5f8f9");
	 $(select).css("color","#e17009");
}

function dialog_confirm_request(title, msg,url) {
    var txt = "<p>" + msg + "</p>";
    $("#dialog_msg").dialog({
        title: title,
        autoOpen: false,
        modal: true,
        width: 400,
        position: ['center', 'middle'],
        draggable : false,
        buttons: [
            {
                text: "Yes",
                click: function() {
                    $(this).dialog("close");
                    request(url);
                }
            },
            {
                text: "No",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
    $("#dialog_msg").html(txt);
    $("#dialog_msg").dialog("open");
}

function dialog_successful__return_action(msg) {
    var txt = "<p>" + msg + "</p>";
    dialog_inform_return_action(txt, "Complete");
}
function dialog_inform_return_action(txt, title) {
    $("#dialog_msg").dialog({
        title: title,
        autoOpen: false,
        modal: true,
        width: 400,
        draggable : false,
        position: ['center', 'middle'],
        buttons: [
            {
                text: "OK",
                autofocus:'autofocus',
                click: function() {
                    $(this).dialog("close");
                    completeAction();
                }
            }
        ]
    });
    $("#dialog_msg").html(txt);
    $("#dialog_msg").dialog({
        close: function(event, ui) {$(this).dialog("close");completeAction();},
    });
    $("#dialog_msg").dialog("open");
}
function TRIM(str){
	return str.replace(/^\s+|\s+$/gm,'');
}