if(localStorage.getItem('keySearch') !== "undefined"){
	$(".keySearchDescription").val(localStorage.getItem('keySearch'));
}
$(document).ready(function(){
	function getByDescription() {
		$.ajax({
			url : 'viewTransportCompProfile/searchTransportCompProfile?description='+$("#txbDescription").val(),
			method : 'POST',
			contentType : 'application/json',
			success : function(data) {
				$('#result-table').replaceWith(data);
			}
		})
	};
	
	$("#btnSearchTransport").click(function(){
		if($("#txbDescription").val()==""){
			$("#messageDescription").show();
			return false;
		}else{
			localStorage.setItem('keySearch',$(".keySearchDescription").val());
			getByDescription();
			$("#messageDescription").hide();
			return true;
		}
	});
	
	$("#btnSaveTransport").click(function(){
		var datas = {
			id: $("#txbTransportCompProfileId").val(),
			description : $("#txbDescription").val(),
			hmacKey : $("#txbHmacKey").val()
		}
		if(checkSave()){
			$.ajax({
				url : 'editTransportCompProfile',
				method : 'POST',
				contentType : 'application/json',
				data : JSON.stringify(datas),
				success: function(){
					alert("successfully!");
					return true;
				},
				error: function(){
					alert("false");
					return false;
				}
			})
		}
	});
	
	$("#btnClearViewTransport").click(function(){
		localStorage.removeItem('keySearch');
		location.reload();
	})
	
	$("#btnClearTransport").click(function(){
		$("#txbDescription").val("");
		$("#txbHmacKey").val("");
	});
	
	$("#btnCreateHmacKey").click(function(){
		$("#messageHmacKey").hide();
		$.ajax({
			url : 'generateHmacKey',
			method : 'GET',
			contentType : 'application/json',
			success : function(data) {
				$('#txbHmacKey').replaceWith(data);
			}
		})
	});
	
	function checkSave(){
		if($("#txbDescription").val()=="" || $("#txbHmacKey").val()==""){
			if($("#txbDescription").val()==""){
				$("#messageDescription").show();
			}
			if($("#txbHmacKey").val()==""){
				$("#messageHmacKey").show();
			}
			return false;
		}else{
			$("#messageDescription").hide();
			$("#messageHmacKey").hide();
			return true;
		}
	};
});