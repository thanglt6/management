$("document").ready(function() {
	highlight($('#menu_matching'));
	$(".scrollTable").css({
		'width' : '100%'
	});
	responData();
});
function setOldToNew() {
	doSearch('');
}
function responData() {

	var popUp = $('#popUp').val();

	if (popUp == 'true') {
		dialog_successful__return_action("[[#{matched.sucess}]]", "[[#{dialog.complete.title}]]");
	}
	if (popUp == 'false') {
		errorMsg("[[#{msg_unexpected_error}]]");
	}
}

function pagingation(pageurl) {
	doSearch(pageurl);
};

function getActUrlForDoSearch(act) {
	 var urlString = window.location.href;
	 var url = new URL(urlString);
	 var purchasingReceiptId = url.searchParams
	 .get("purchasingReceiptId");
	return "/astari-qr-bo/matching/searchPopup?purchasingReceiptId"+purchasingReceiptId + act;
}

function buttonClick(transaction) {
	try {
		var tranIdForModel = document.getElementById('tranIdForModel');
		var transactionDate = transaction.parentElement.parentNode.cells[0].innerText;
		var affiliatedStoreNo = transaction.parentElement.parentNode.cells[1].innerText;
		var storeNameKanji = transaction.parentElement.parentNode.cells[2].innerText;
		var storeNameKana = transaction.parentElement.parentNode.cells[3].innerText;
		var transactionAmount = transaction.parentElement.parentNode.cells[4].innerText;
		var terminalNo = transaction.parentElement.parentNode.cells[5].innerText;
		var appUserId = transaction.parentElement.parentNode.cells[6].innerText;
		var transactionId = transaction.parentElement.parentNode.cells[7].innerText;

		var transactionDateView = document.getElementById('transactionDate');
		var affiliatedStoreNoView = document
				.getElementById('affiliatedStoreNo');
		var storeNameKanjiView = document.getElementById('storeNameKana');
		var storeNameKanaView = document.getElementById('storeNameKanji');
		var transactionAmountView = document
				.getElementById('transactionAmount');
		var terminalNoView = document.getElementById('terminalNo');
		var appUserIdView = document.getElementById('appUserId');
		var transactionIdView = document.getElementById('tranId');

		if (transactionDateView != undefined) {
			transactionDateView.innerHTML = transactionDate;
		}
		if (affiliatedStoreNoView != undefined) {
			affiliatedStoreNoView.innerHTML = affiliatedStoreNo;
		}
		if (storeNameKanjiView != undefined) {
			storeNameKanjiView.innerHTML = storeNameKanji;
		}
		if (storeNameKanaView != undefined) {
			storeNameKanaView.innerHTML = storeNameKana;
		}
		if (transactionAmountView != undefined) {
			transactionAmountView.innerHTML = transactionAmount;
		}
		if (terminalNoView != undefined) {
			terminalNoView.innerHTML = terminalNo;
		}
		if (appUserIdView != undefined) {
			appUserIdView.innerHTML = appUserId;
		}
		if (transactionIdView != undefined) {
			transactionIdView.innerHTML = transactionId;

		}
		tranIdForModel.value = transactionId;
		modal.style.display = "none";
	} catch (e) {
		alert('exception' + e)
	}
}
function completeAction() {
	onBack();
}
$(".button").button();
function onBack() {
	location.href = "/astari-qr-bo/matching?stayPage=true";
}
var urlString = window.location.href;
var url = new URL(urlString);
var purchasingReceiptId = url.searchParams.get("purchasingReceiptId");

var modal = document.getElementById('myModal');
var btn = document.getElementById("select");
var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
	modal.style.display = "block";
}
span.onclick = function() {
	modal.style.display = "none";
}
window.onclick = function(event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}
function completeAction() {
	onSearch();
}
$( function() {
	$("#transactionFromDate").datepicker("getDate");
	$( "#transactionFromDate" ).datepicker(({
		dateFormat: "yy/mm/dd",
		changeMonth: true,
		months  : ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'],
		changeYear: true,
		yearRange : '1970:2050',
		maxDate : new Date()
	}));
});
$( function() {
	$("#transactionToDate").datepicker("getDate");
$( "#transactionToDate" ).datepicker(({
		dateFormat: "yy/mm/dd",
		changeMonth: true,
		months  : ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'],
		changeYear: true,
		yearRange : '1970:2050',
		maxDate : new Date()
	}));
});