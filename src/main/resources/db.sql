DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
                         `id` bigint AUTO_INCREMENT NOT NULL,
                         `user_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                         `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                         `deleted` bit(1) NOT NULL,
                         `full_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
                         `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
                         `created_date` datetime DEFAULT NULL,
                         `created_by` varchar(45) COLLATE utf8_bin DEFAULT NULL,
                         `updated_date` datetime DEFAULT NULL,
                         `last_login` datetime DEFAULT NULL,
                         `updated_by` varchar(45) COLLATE utf8_bin DEFAULT NULL,
                         `deleted_by` varchar(45) COLLATE utf8_bin DEFAULT NULL,
                         PRIMARY KEY (`id`) USING BTREE,
                         UNIQUE KEY `username_UNIQUE` (`user_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;
